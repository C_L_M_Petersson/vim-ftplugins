setlocal foldmethod=expr
setlocal foldexpr=GetHaskellFold(v:lnum)


" function! s:IndentLevel {{{
function! s:IndentLevel(lnum)
    return indent(a:lnum) / &shiftwidth
endfunction
" }}}


" function! s:GetNextLineNumber {{{
function! s:GetNextLineNumber(lnum)
    let line = getline(a:lnum)

    if line('$') < a:lnum
        return -2
    elseif line =~? '\v^ *$'
        return s:GetNextLineNumber(a:lnum+1)
    else
        return a:lnum
    endif
endfunction
" }}}


" function! GetHaskellFold {{{
function! GetHaskellFold(lnum)
    let line   = getline(a:lnum)
    let indent = s:IndentLevel(a:lnum)

    if line =~? '\v^ *\S+ *\:\:'
        return '>' . (indent+1)
    elseif line =~? '\v^ *module> +<\S+> *(|\() *$'
        return '>1'
    elseif line =~? '\v^ *\) *where>'
        return '>1'
    elseif line =~? '\v^ *where>'
        return '>' . (indent+1)
    elseif line =~? '\v^ *(data|type) +\u\S*\s*\='
        return '>' . (indent+1)
    elseif line =~? '\v^ *instance>.*<where>'
        return '>' . (indent+1)
    elseif line =~? '\v^ *$'
        let nextlnum = s:GetNextLineNumber(a:lnum+1)
        let nextline = getline(nextlnum)
        if nextlnum == -2
            return '='
        elseif nextline =~? '\v^ *\S+ *\:\:'
            return s:IndentLevel(nextlnum)
        elseif nextline =~? '\v^ *(data|type) +\u\S*\s*\='
            return s:IndentLevel(nextlnum)
        elseif nextline =~? '\v^ *instance>.*<where>'
            return s:IndentLevel(nextlnum)
        else
            return '='
        endif
    else
        return '='
    endif
endfunction
" }}}
