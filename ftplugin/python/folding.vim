setlocal foldmethod=expr
setlocal foldexpr=GetPythonFold(v:lnum)

" function! IndentLevel {{{
function! IndentLevel(lnum)
    return indent(a:lnum) / &shiftwidth
endfunction
" }}}

" function! NextNonBlankLine {{{
function! s:NextNonBlankLine(lnum)
    let numlines = line('$')
    let current = a:lnum + 1

    while current <= numlines
        if getline(current) =~? '\v\S' "&& getline(current) !~? '\v^ *\#'
            return current
        endif

        let current += 1
    endwhile

    return -2
endfunction
" }}}

" function! GetFortranFold {{{
function! GetPythonFold(lnum)
    let line = getline(a:lnum)
    let lnumNext = s:NextNonBlankLine(a:lnum)

    if line =~? '\v\:$'
        return IndentLevel(a:lnum)+1
    elseif line !~? '\v\S'
        if lnumNext == -2
            return 0
        elseif IndentLevel(lnumNext) =~? '\v(>|)0'
            return 0
        else
            return "="
        endif
    else
        return IndentLevel(a:lnum)
    endif
endfunction
" }}}
