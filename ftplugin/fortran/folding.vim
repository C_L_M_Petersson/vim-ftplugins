setlocal foldmethod=expr
setlocal foldexpr=GetFortranFold(v:lnum)

" function! s:CaseAbove {{{
function! s:CaseAbove(lnum)

    if a:lnum == 1
        return 0
    endif

    let line = getline(a:lnum-1)

    if line =~? '\v^ *case( +default>| *\(.+\))'
        return 1
    elseif line =~? '\v^ *select +case>'
        return 0
    else
        return CaseAbove(a:lnum-1)
    endif
endfunction
" }}}

" function! s:Terminates {{{
function! s:Terminates(lnum, expr)
    let line = getline(a:lnum)

    if line =~? '\v' . a:expr . ' *(\!|$)'
        return 1
    elseif line =~? '\v\& *(\!|$)'
        return s:Terminates(a:lnum+1, a:expr)
    else
        return 0
    endif
endfunction
" }}}

" function! GetFortranFold {{{
function! GetFortranFold(lnum)
    let line = getline(a:lnum)
    let lineprev = getline(a:lnum-1)
    let linenext = getline(a:lnum+1)

    if line =~? '\v^ *(module|program)>'
        return '>1'
    elseif line =~? '\v^ *contains>'
        return '>1'
    elseif lineprev =~? '\v^ *end *(module|program)>'
        return '0'

    elseif line =~? '\v^ *end *(function|subroutine)>'
        return 2
    elseif line =~? '\v^[A-Za-z ]*(function|subroutine)>'
        return '>2'
    elseif line =~? '\v^ *\S+ *[A-Za-z ]*function>'
        return '>2'
    elseif lineprev =~? '\v^ *end *(function|subroutine)>'
        return 1

    elseif line =~? '\v^ *if>' && s:Terminates(a:lnum, '<then>')
        return "a1"
    elseif line =~? '\v^ *else *(if|)>'
        return "a1"
    elseif linenext =~? '\v^ *else *(if|)>'
        return "s1"
    elseif line =~? '\v^ *(\S+\:|) *do'
        return "a1"
    elseif line =~? '\v^ *end *(if|do)>'
        return "s1"

    elseif line =~? '\v^ *select +case>'
        return "a1"
    elseif line =~? '\v^ *case( +default>| *\(.+\))'
        return "a1"
    elseif linenext =~? '\v^ *case( +default>| *\(.+\))'
        if s:CaseAbove(a:lnum)
            return "s1"
        endif
    elseif linenext =~? '\v^ *end +select>'
        if s:CaseAbove(a:lnum)
            return "s1"
        endif
    elseif line =~? '\v^ *end +select>'
        return "s1"

    elseif line =~? '\v^ *type>' && line !~? '\:\:'
        return "a1"
    elseif line =~? '\v *end +type>'
        return "s1"

    endif

    return "="
endfunction
" }}}
