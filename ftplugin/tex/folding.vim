setlocal foldmethod=expr
setlocal foldexpr=GetTexFold(v:lnum)

" function! GetFortranFold {{{
function! g:GetTexFold(lnum)
    let line = getline(a:lnum)

    if a:lnum == 0
        return 0
    elseif line =~? '\v^ *\\begin>'
        return "a1"
    elseif line =~? '\v^ *\\end>'
        return "s1"
    else
        return "="
    endif
endfunction
" }}}
