" Declarations {{{
" function! s:Declare {{{
function! s:Declare(type)
    if a:type == 0
        let start = "if"
        let end = "endif"
        let input = input("Please give conditional: ")

    elseif a:type == 1
        let start = "for"
        let end = "endfor"
        let input = input("Please give iterative variable: ") . " in [" . input("Please give list: ") . "]"

    elseif a:type == 2
        let start = "while"
        let end = "endwhile"
        let input = input("Please give conditional: ")

    elseif a:type == 3
        let start = "function"
        let end = "endfunction"
        let input = input("Please give function name: ") . "(" . input("Please give input variables: ") . ")"

    endif

    execute "normal! i" . start . input . "\<cr>" . end . "\<esc>k"

    let indentation = indent(line('.')) + &shiftwidth
    execute "normal! A\<cr>\<esc>" . indentation . "a \<esc>"
    startinsert!
endfunction
" }}}

" Key mappings {{{
iabbrev iff    <Esc>:call <SID>Declare(0)<cr>
iabbrev forl   <Esc>:call <SID>Declare(1)<cr>
iabbrev whilel <Esc>:call <SID>Declare(2)<cr>
iabbrev func   <Esc>:call <SID>Declare(3)<cr>
" }}}
" }}}
