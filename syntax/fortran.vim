syntax keyword PetscType PetscErrorCode PetscMPIInt PetscInt PetscScalar
    \ PetscReal PetscLogDouble Mat Vec VecScatter EPS

highlight link PetscType Type
