syntax keyword PetscType PetscErrorCode PetscMPIInt PetscInt PetscScalar
    \ PetscReal Mat Vec VecScatter EPS

highlight link PetscType Type
